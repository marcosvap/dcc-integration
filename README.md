Este é um projeto java com spring boot.

O ambiente de desenvolvimento necessário é:

java 8+
maven
mysql server

para gerar um novo build basta utilizar o seguinte comando:

-mvn clean install

o artefato novo estará dentro da pasta target.

para rodar o projeto basta executar o arquivo .JAR gerado pelo comando acima com o seguinte comando.

-java -jar nome_jar.jar

Existem 2 arquivos de configuração onde está setado a porta em que a job de integração irá rodar, os dados de acesso ao banco de dados da job de integração e os dados de acesso ao banco da DCC. Os arquivos são:
-application.properties (porta e dados de conexão do banco local)
-beans.xml (dados de conexão com o banco da dcc)

A job está setada para rodar todos os dias 1 A.M, para alterar isso basta mudar o cron da job na classe (arquivo) IntegrationTask.class. Para gerar um novo cron basta acessar o site:

http://www.cronmaker.com/

O projeto possui 2 tabelas de de-para ( dicionário ) que são os id's da base de dados da DCC e os id's da base de dados da Gamific. Caso haja qualquer alteração nesses id's ou seja necessário cadastrar algum registro novo ( por motivo de um funcionário novo entrar ou algum funcionário sair, ou ainda cadastrar novas métricas por exemplo ) são disponibilizadas api's RestFull para executar essas ações ou ainda é possível alterar direto no banco de dados. Seguem as api's

-PLAYER ( VENDEDORES )
URI DO PROJETO/playerDicionary ( serviço para buscar todos os registros ) METODO GET
URI DO PROJETO/playerDicionary/{id} ( serviço para buscar 1 registro especifico pelo id ) METODO GET
URI DO PROJETO/playerDicionary/search/findByDccPlayerId?dccPlayerId={dccPlayerId} ( serviço para buscar 1 registro especifico pelo id do vendedor na base da DCC ) METODO GET
URI DO PROJETO/playerDicionary/{id} ( serviço para atualizar 1 registro especifico pelo id ) METODO PUT
- parametros devem ser enviados pelo body no formato json. Exemplo:
{
	"dccPlayerId" : "",
	"gamificPlayerId" : ""
}
URI DO PROJETO/playerDicionary ( serviço para criar 1 registro) METODO POST
- parametros devem ser enviados pelo body no formato json. Exemplo:
{
	"dccPlayerId" : "",
	"gamificPlayerId" : ""
}

-METRIC ( PRODUTOS )
URI DO PROJETO/metricDicionary ( serviço para buscar todos os registros ) METODO GET
URI DO PROJETO/metricDicionary/{id} ( serviço para buscar 1 registro especifico pelo id ) METODO GET
URI DO PROJETO/metricDicionary/search/findByDccMetricId?dccMetricId={dccMetricId} ( serviço para buscar 1 registro especifico pelo id do produto na base da DCC ) METODO GET
URI DO PROJETO/metricDicionary/{id} ( serviço para atualizar 1 registro especifico pelo id ) METODO PUT
- parametros devem ser enviados pelo body no formato json. Exemplo:
{
	"gamificMetricId" : "",
	"dccMetricId" : ""
}
URI DO PROJETO/metricDicionary ( serviço para criar 1 registro) METODO POST
- parametros devem ser enviados pelo body no formato json. Exemplo:
{
	"gamificMetricId" : "",
	"dccMetricId" : ""
}

Além das tabelas de de-para também existe uma tabela de parametros de sistema onde pode ser editada a query que será usada como busca na base de dados da DCC e a campanha atual onde serão inseridas as vendas na gamific.

A campanha atual é um campo opcional, e deve ser preenchida com o id da campanha na gamific ( é possível pegar esse id entrando no sistema administrador da gamific ou solicitando para a equipe de suporte da gamific ). Por default se esse campo não for preenchido será inserido na campanha de vendas principal do mês que é criada todo mês automaticamente pelo sistema da gamific.

A query que será usada na busca da base da DCC também pode ser alterada sem impactar a job de integração, porém se for alterado algum nome de campo que a query retorna, deve-se alterar também o código fonte da integração e subir uma nova versão. Na clausula WHERE o filtro de data da query deve ser substituido pelo seguinte:
A data inicial na query deve ser a chave "!@#" sem as aspas.
A data final na query deve ser a chave "#@!" sem as aspas.

Nunca deve-se inserir um novo registro nessa tabela, apenas atualizar o unico registro existente.

Para alterar os registros dessa tabela pode-se alterar diretamente no banco de dados ou então via api RestFull:

-SYSTEM PARAMETER ( PARAMETROS DE SISTEMA )
URI DO PROJETO/systemParameter ( serviço para buscar todos os registros ) METODO GET
URI DO PROJETO/systemParameter/{id} ( serviço para buscar 1 registro especifico pelo id ) METODO GET
URI DO PROJETO/systemParameter/{id} ( serviço para atualizar 1 registro especifico pelo id ) METODO PUT
- parametros devem ser enviados pelo body no formato json. Exemplo:
{
	"integrationQuery" : "",
	"currentEpisodeId" : ""
}

É possível forçar a integração chamando a API RestFull manualmente caso o tempo definido para rodar a job seja muito grande e haja necessidade de atualizar os dados na gamfic urgentemente por exemplo. Basta chamar o seguinte serviço:

URI DO PROJETO/integration/synchronize METODO GET