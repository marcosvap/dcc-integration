package com.dcc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "EPISODE")
public class EpisodeEntity {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "GAMIFIC_EPISODE_ID")
	@NotNull
	private String gamificEpisodeId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGamificEpisodeId() {
		return gamificEpisodeId;
	}

	public void setGamificEpisodeId(String gamificEpisodeId) {
		this.gamificEpisodeId = gamificEpisodeId;
	}

}