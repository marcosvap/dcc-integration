package com.dcc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "PLAYER_DICIONARY")
public class PlayerDicionaryEntity {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "DCC_PLAYER_ID")
	@NotNull
	private String dccPlayerId;

	@Column(name = "GAMIFIC_PLAYER_ID")
	@NotNull
	private String gamificPlayerId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDccPlayerId() {
		return dccPlayerId;
	}

	public void setDccPlayerId(String dccPlayerId) {
		this.dccPlayerId = dccPlayerId;
	}

	public String getGamificPlayerId() {
		return gamificPlayerId;
	}

	public void setGamificPlayerId(String gamificPlayerId) {
		this.gamificPlayerId = gamificPlayerId;
	}

}