package com.dcc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "METRIC_DICIONARY")
public class MetricDicionaryEntity {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "GAMIFIC_METRIC_ID")
	@NotNull
	private String gamificMetricId;

	@Column(name = "DCC_METRIC_ID")
	@NotNull
	private String dccMetricId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGamificMetricId() {
		return gamificMetricId;
	}

	public void setGamificMetricId(String gamificMetricId) {
		this.gamificMetricId = gamificMetricId;
	}

	public String getDccMetricId() {
		return dccMetricId;
	}

	public void setDccMetricId(String dccMetricId) {
		this.dccMetricId = dccMetricId;
	}

}