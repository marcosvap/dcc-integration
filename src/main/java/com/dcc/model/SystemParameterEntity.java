package com.dcc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SYSTEM_PARAMETER")
public class SystemParameterEntity {

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "THREAD_FREQUENCY")
	@NotNull
	private Long threadFrequency;

	@Column(columnDefinition = "LONGTEXT", name = "INTEGRATION_QUERY")
	@NotNull
	private String integrationQuery;

	@Column(name = "LAST_RUN_DATE")
	@NotNull
	private Date lastRunDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getThreadFrequency() {
		return threadFrequency;
	}

	public void setThreadFrequency(Long threadFrequency) {
		this.threadFrequency = threadFrequency;
	}

	public String getIntegrationQuery() {
		return integrationQuery;
	}

	public void setIntegrationQuery(String integrationQuery) {
		this.integrationQuery = integrationQuery;
	}

	public Date getLastRunDate() {
		return lastRunDate;
	}

	public void setLastRunDate(Date lastRunDate) {
		this.lastRunDate = lastRunDate;
	}

}