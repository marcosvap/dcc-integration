package com.dcc.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dcc.model.EpisodeEntity;

@RepositoryRestResource(collectionResourceRel = "episode", path = "episode")
public interface EpisodeRepository extends PagingAndSortingRepository<EpisodeEntity, Long> {

}