package com.dcc.repositories;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

import com.dcc.model.MetricDicionaryEntity;
import com.dcc.model.PlayerDicionaryEntity;
import com.dcc.model.SystemParameterEntity;

@Configuration
public class RepositoryConfig extends RepositoryRestConfigurerAdapter {
	@Override
	public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
		config.exposeIdsFor(MetricDicionaryEntity.class, PlayerDicionaryEntity.class, SystemParameterEntity.class);
	}
}