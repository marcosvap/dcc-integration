package com.dcc.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dcc.model.MetricDicionaryEntity;

@RepositoryRestResource(collectionResourceRel = "metricDicionary", path = "metricDicionary")
public interface MetricDicionaryRepository extends PagingAndSortingRepository<MetricDicionaryEntity, Long> {

	MetricDicionaryEntity findByDccMetricId(@Param("dccMetricId") String dccMetricId);

}