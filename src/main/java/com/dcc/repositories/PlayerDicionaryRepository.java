package com.dcc.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dcc.model.PlayerDicionaryEntity;

@RepositoryRestResource(collectionResourceRel = "playerDicionary", path = "playerDicionary")
public interface PlayerDicionaryRepository extends PagingAndSortingRepository<PlayerDicionaryEntity, Long> {

	PlayerDicionaryEntity findByDccPlayerId(@Param("dccPlayerId") String dccPlayerId);

}