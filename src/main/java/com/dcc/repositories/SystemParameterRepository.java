package com.dcc.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.dcc.model.SystemParameterEntity;

@RepositoryRestResource(collectionResourceRel = "systemParameter", path = "systemParameter")
public interface SystemParameterRepository extends PagingAndSortingRepository<SystemParameterEntity, Long> {

}