package com.dcc.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.dcc.dto.DCCSaleDTO;
import com.dcc.dto.GamificInputDTO;
import com.dcc.dto.GamificResponseDTO;
import com.dcc.jdbc.SaleJDBCTemplate;
import com.dcc.model.EpisodeEntity;
import com.dcc.model.MetricDicionaryEntity;
import com.dcc.model.PlayerDicionaryEntity;
import com.dcc.model.SystemParameterEntity;
import com.dcc.repositories.EpisodeRepository;
import com.dcc.repositories.MetricDicionaryRepository;
import com.dcc.repositories.PlayerDicionaryRepository;
import com.dcc.repositories.SystemParameterRepository;
import com.google.common.collect.Lists;

@RestController
@EnableAutoConfiguration
public class IntegrationController {

	@Autowired
	private MetricDicionaryRepository metricDicionaryRepository;

	@Autowired
	private PlayerDicionaryRepository playerDicionaryRepository;

	@Autowired
	private SystemParameterRepository systemParameterRepository;

	@Autowired
	private EpisodeRepository episodeRepository;

	@Value("${gamific.integration.url}")
	public String GAMIFIC_INTEGRATION_URL;

	private final Logger logger = Logger.getLogger(IntegrationController.class);

	private HttpHeaders configureHeaders() {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("ApiKey", "769FAAD9AC558D48198BED234674B");

		return headers;
	}

	/**
	 * @Javadoc Does the synchronization.
	 * 
	 * @author Marcos V. Aguiar
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "integration/synchronize", method = RequestMethod.GET, produces = "application/json")
	public void synchronizeDB() throws Exception {

		try {
			SystemParameterEntity systemParameter = systemParameterRepository.findOne(1l);

			RestTemplate restTemplate = new RestTemplate();

			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

			SaleJDBCTemplate saleJDBCTemplate = (SaleJDBCTemplate) context.getBean("saleJDBCTemplate");

			Date currentDate = new Date();

			Date startDate = systemParameter.getLastRunDate();

			String currentDateString = new SimpleDateFormat("yyyy-MM-dd").format(currentDate);

			String startDateString = new SimpleDateFormat("yyyy-MM-dd").format(startDate);

			String query = systemParameter.getIntegrationQuery();

			query = query.replaceAll("'!@#'", "'" + startDateString + "'");
			query = query.replaceAll("'#@!'", "'" + currentDateString + "'");

			logger.log(Priority.INFO, "query utilizada: " + query);

			systemParameter.setLastRunDate(currentDate);
			systemParameterRepository.save(systemParameter);

			List<DCCSaleDTO> sales = saleJDBCTemplate.listSales(query);

			logger.log(Priority.INFO, "tamanho da lista retornada pelo select: " + sales.size());

			List<GamificInputDTO> inputs = new ArrayList<GamificInputDTO>();

			for (DCCSaleDTO sale : sales) {

				List<EpisodeEntity> episodes = Lists.newArrayList(episodeRepository.findAll());

				GamificInputDTO input = new GamificInputDTO();

				PlayerDicionaryEntity playerDictionary = playerDicionaryRepository
						.findByDccPlayerId(sale.getIdPessoa());

				MetricDicionaryEntity metricDictionary = metricDicionaryRepository
						.findByDccMetricId(sale.getIdProduto());

				episodes.forEach(episode -> {
					if (playerDictionary != null && metricDictionary != null) {
						input.setDeltaPoints(sale.getVlDocumento());
						input.setEpisodeId(episode.getGamificEpisodeId());
						input.setMetricId(metricDictionary.getGamificMetricId());
						input.setPlayerId(playerDictionary.getGamificPlayerId());
						inputs.add(input);
					}
				});

			}

			logger.log(Priority.INFO, "tamanho da lista para importação: " + inputs.size());

			for (GamificInputDTO gamificInputDTO : inputs) {

				if (gamificInputDTO.getDeltaPoints() != null && gamificInputDTO.getEpisodeId() != null
						&& gamificInputDTO.getMetricId() != null && gamificInputDTO.getPlayerId() != null) {

					ResponseEntity<GamificResponseDTO> resp;

					String points = "";

					if (gamificInputDTO.getDeltaPoints().contains(".")) {
						points = gamificInputDTO.getDeltaPoints().substring(0,
								gamificInputDTO.getDeltaPoints().indexOf("."));
						logger.log(Priority.INFO, points);
					} else {
						points = gamificInputDTO.getDeltaPoints();
						logger.log(Priority.INFO, points);
					}

					String reqURI = GAMIFIC_INTEGRATION_URL + "?episodeId=" + gamificInputDTO.getEpisodeId()
							+ "&metricId=" + gamificInputDTO.getMetricId() + "&deltaPoints=" + points + "&playerId="
							+ gamificInputDTO.getPlayerId();

					logger.log(Priority.INFO, reqURI);

					HttpEntity<?> httpEntity = new HttpEntity<Object>(null, configureHeaders());

					resp = restTemplate.exchange(reqURI, HttpMethod.POST, httpEntity,
							new ParameterizedTypeReference<GamificResponseDTO>() {
							});

					if (resp != null) {
						logger.log(Priority.INFO, resp.getBody());
						if (resp.getStatusCodeValue() == HttpStatus.OK.value()) {
							logger.log(Priority.INFO, "sucesso para o jogador: " + gamificInputDTO.getPlayerId());
						} else {
							logger.error("Error calling for gamific service\" + resp.getStatusCodeValue()");
							throw new Exception("Error calling for gamific service" + resp.getStatusCodeValue());
						}
					} else {
						logger.log(Priority.INFO, "response null");
					}
				}

			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}

	}

}