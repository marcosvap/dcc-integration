package com.dcc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class DCCTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(DCCTaskApplication.class, args);
	}
}
