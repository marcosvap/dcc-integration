package com.dcc.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GamificInputDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3331104824283130602L;

	@JsonProperty("episode_id")
	private String episodeId;

	@JsonProperty("metric_id")
	private String metricId;

	@JsonProperty("player_id")
	private String playerId;

	@JsonProperty("delta_points")
	private String deltaPoints;

	public String getEpisodeId() {
		return episodeId;
	}

	public void setEpisodeId(String episodeId) {
		this.episodeId = episodeId;
	}

	public String getMetricId() {
		return metricId;
	}

	public void setMetricId(String metricId) {
		this.metricId = metricId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}

	public String getDeltaPoints() {
		return deltaPoints;
	}

	public void setDeltaPoints(String deltaPoints) {
		this.deltaPoints = deltaPoints;
	}

}