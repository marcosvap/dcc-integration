package com.dcc.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DCCSaleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8804957135464613659L;

	@JsonProperty("nr_documento")
	private String nrDocumento;

	@JsonProperty("al_acrescimo")
	private String alAcrescimo;

	@JsonProperty("al_desconto")
	private String alDesconto;

	@JsonProperty("dt_emissao")
	private String dtEmissao;

	@JsonProperty("vl_acrescimo")
	private String vlAcrescimo;

	@JsonProperty("vl_desconto")
	private String vlDesconto;

	@JsonProperty("nr_nfse")
	private String nrNFSE;

	@JsonProperty("vl_documento")
	private String vlDocumento;

	@JsonProperty("id_produto")
	private String idProduto;

	@JsonProperty("nm_produto")
	private String nmProduto;

	@JsonProperty("qt_item")
	private String qtItem;

	@JsonProperty("vl_unitario")
	private String vlUnitario;

	@JsonProperty("cd_empresa")
	private String cdEmpresa;

	@JsonProperty("id_pessoa")
	private String idPessoa;

	@JsonProperty("nm_pessoa")
	private String nmPessoa;

	@JsonProperty("nm_operacao")
	private String nmOperacao;

	@JsonProperty("cd_chamada")
	private String cdChamada;

	public String getNrDocumento() {
		return nrDocumento;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public String getAlAcrescimo() {
		return alAcrescimo;
	}

	public void setAlAcrescimo(String alAcrescimo) {
		this.alAcrescimo = alAcrescimo;
	}

	public String getAlDesconto() {
		return alDesconto;
	}

	public void setAlDesconto(String alDesconto) {
		this.alDesconto = alDesconto;
	}

	public String getDtEmissao() {
		return dtEmissao;
	}

	public void setDtEmissao(String dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	public String getVlAcrescimo() {
		return vlAcrescimo;
	}

	public void setVlAcrescimo(String vlAcrescimo) {
		this.vlAcrescimo = vlAcrescimo;
	}

	public String getVlDesconto() {
		return vlDesconto;
	}

	public void setVlDesconto(String vlDesconto) {
		this.vlDesconto = vlDesconto;
	}

	public String getNrNFSE() {
		return nrNFSE;
	}

	public void setNrNFSE(String nrNFSE) {
		this.nrNFSE = nrNFSE;
	}

	public String getVlDocumento() {
		return vlDocumento;
	}

	public void setVlDocumento(String vlDocumento) {
		this.vlDocumento = vlDocumento;
	}

	public String getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(String idProduto) {
		this.idProduto = idProduto;
	}

	public String getNmProduto() {
		return nmProduto;
	}

	public void setNmProduto(String nmProduto) {
		this.nmProduto = nmProduto;
	}

	public String getQtItem() {
		return qtItem;
	}

	public void setQtItem(String qtItem) {
		this.qtItem = qtItem;
	}

	public String getVlUnitario() {
		return vlUnitario;
	}

	public void setVlUnitario(String vlUnitario) {
		this.vlUnitario = vlUnitario;
	}

	public String getCdEmpresa() {
		return cdEmpresa;
	}

	public void setCdEmpresa(String cdEmpresa) {
		this.cdEmpresa = cdEmpresa;
	}

	public String getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(String idPessoa) {
		this.idPessoa = idPessoa;
	}

	public String getNmPessoa() {
		return nmPessoa;
	}

	public void setNmPessoa(String nmPessoa) {
		this.nmPessoa = nmPessoa;
	}

	public String getNmOperacao() {
		return nmOperacao;
	}

	public void setNmOperacao(String nmOperacao) {
		this.nmOperacao = nmOperacao;
	}

	public String getCdChamada() {
		return cdChamada;
	}

	public void setCdChamada(String cdChamada) {
		this.cdChamada = cdChamada;
	}

}