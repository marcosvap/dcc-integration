package com.dcc.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GamificResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4396176272649192145L;
}