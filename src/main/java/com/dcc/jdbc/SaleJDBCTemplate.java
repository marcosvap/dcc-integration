package com.dcc.jdbc;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.dcc.dto.DCCSaleDTO;

public class SaleJDBCTemplate {

	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(dataSource);
	}

	public List<DCCSaleDTO> listSales(String query) {
		String SQL = query;
		List<DCCSaleDTO> sales = jdbcTemplateObject.query(SQL, new SaleMapper());
		return sales;
	}

}