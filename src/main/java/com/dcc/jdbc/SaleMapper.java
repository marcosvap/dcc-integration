package com.dcc.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.dcc.dto.DCCSaleDTO;

public class SaleMapper implements RowMapper<DCCSaleDTO> {

	public DCCSaleDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
		DCCSaleDTO sale = new DCCSaleDTO();

		sale.setIdPessoa(rs.getString("IdRepresentante"));
		sale.setNrDocumento(rs.getString("NrDocumento"));
		sale.setAlAcrescimo(rs.getString("AlAcrescimo"));
		sale.setAlDesconto(rs.getString("AlDesconto"));
		sale.setDtEmissao(rs.getString("DtEmissao"));
		sale.setVlAcrescimo(rs.getString("VlAcrescimo"));
		sale.setVlDesconto(rs.getString("VlDesconto"));
		sale.setNrNFSE(rs.getString("NrNFSE"));
		sale.setVlDocumento(rs.getString("VlDocumento"));
		sale.setIdProduto(rs.getString("CodProduto"));
		sale.setNmProduto(rs.getString("NmProduto"));
		sale.setQtItem(rs.getString("QtItem"));
		sale.setVlUnitario(rs.getString("VlUnitario"));
		sale.setNmPessoa(rs.getString("NmCliente"));

		return sale;
	}

}