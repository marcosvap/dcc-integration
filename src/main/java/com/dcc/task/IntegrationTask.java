package com.dcc.task;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.dcc.controller.IntegrationController;
import com.dcc.repositories.SystemParameterRepository;

@Component
public class IntegrationTask {

	@Autowired
	private IntegrationController integrationController;

	@Autowired
	private SystemParameterRepository systemParameterRepository;

	private Long cron = 0L;

	public IntegrationTask() {
		/**
		 * SystemParameterEntity systemParameter =
		 * systemParameterRepository.findOne(1l);
		 * 
		 * this.cron = systemParameter.getThreadFrequency();
		 **/
	}

	private final Logger log = Logger.getLogger(IntegrationTask.class);

	// running everyday at 1 am
	@Scheduled(cron = "0 1 * * * *")
	public void doIntegration() {

		try {
			integrationController.synchronizeDB();
		} catch (Exception e) {
			log.error("Error IntegrationTask. Message: ", e);
		}

	}

}